import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:pizza/config.dart';
import 'package:pizza/pizza.dart';
import 'package:pizza/cart_manager.dart';
import 'package:pizza/functions_api.dart';
import 'package:provider/provider.dart';

class MasterScreen extends ChangeNotifierProvider {
  MasterScreen({super.key, required super.create});

  final _pizzaTfController = TextEditingController();

  // @override
  // void dispose() {
  //   _pizzaTfController.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: Row(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {
                      context.go("/");
                    },
                    child: const Text(
                      "Ma Pizzeria du 54",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Consumer<CartManager>(
                      builder: (context, cart, child) {
                        return ElevatedButton(
                          child: Text(
                            "Voir mon panier (${context.watch<CartManager>().numberOfProductsChosen})",
                          ),
                          onPressed: () {
                            context.go("/cart");
                          },
                        );
                      },
                    ),
                    const SizedBox(width: 10),
                  ],
                ),
              ),
            ],
          ),
        ),
        body: Center(
          child: Column(
            children: [
              Form(
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      controller: _pizzaTfController,
                      decoration: const InputDecoration(
                        hintText: 'Recherchez une pizza',
                      ),
                      validator: (String? value) {
                        if (value == null || value.isEmpty) {
                          return 'Entrez une pizza';
                        }
                        return null;
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: ElevatedButton(
                        onPressed: () {
                          context.go("/search/${_pizzaTfController.text}");
                        },
                        child: const Text('Rechercher'),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  const SizedBox(
                    width: 10,
                  ),
                  const Text("Trier par bases :"),
                  const SizedBox(
                    width: 10,
                  ),
                  ElevatedButton(
                    child: const Text('Tomate'),
                    onPressed: () {
                      context.go("/list/Tomate");
                    },
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  ElevatedButton(
                    child: const Text('Crème'),
                    onPressed: () {
                      context.go("/list/Crème");
                    },
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  ElevatedButton(
                    child: const Text('Nature'),
                    onPressed: () {
                      context.go("/list/Nature");
                    },
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Expanded(
                child: FutureBuilder(
                    future: getAllPizzas(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        final List<Pizza> pizzas = snapshot.data!;
                        return ListView.builder(
                          itemCount: pizzas.length,
                          itemBuilder: (context, index) {
                            final Pizza pizza = pizzas[index];
                            return ListTile(
                              leading: Image.network(
                                  "$apiPizzaUrl/assets/${pizza.image}"),
                              title: Text("${pizza.name}"),
                              subtitle: Row(
                                children: [
                                  Text(
                                    "Base ${pizza.base} - ",
                                    style: const TextStyle(
                                      fontStyle: FontStyle.italic,
                                    ),
                                  ),
                                  Text("${pizza.price} €"),
                                ],
                              ),
                              trailing: IconButton(
                                icon: const Icon(Icons.add),
                                onPressed: () {
                                  Provider.of<CartManager>(context,
                                          listen: false)
                                      .addToCart(pizza);
                                },
                              ),
                              onTap: () {
                                context.go("/detail/${pizza.id}");
                              },
                            );
                          },
                        );
                      } else if (snapshot.hasError) {
                        if (kDebugMode) {
                          print(snapshot.error);
                        }
                        return const Text("Erreur");
                      } else {
                        return const CircularProgressIndicator();
                      }
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
