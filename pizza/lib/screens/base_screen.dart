import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:go_router/go_router.dart';
import 'package:pizza/config.dart';
import 'package:pizza/pizza.dart';
import 'package:pizza/cart_manager.dart';
import 'package:pizza/functions_api.dart';
import 'package:provider/provider.dart';

class BaseScreen extends ChangeNotifierProvider {
  final String base;

  BaseScreen({super.key, required this.base, required super.create});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: Row(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {
                      context.go("/");
                    },
                    child: const Text(
                      "Ma Pizzeria du 54",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Consumer<CartManager>(
                      builder: (context, cart, child) {
                        return ElevatedButton(
                          child: Text(
                            "Voir mon panier (${context.watch<CartManager>().numberOfProductsChosen})",
                          ),
                          onPressed: () {
                            context.go("/cart");
                          },
                        );
                      },
                    ),
                    const SizedBox(width: 10),
                  ],
                ),
              ),
            ],
          ),
        ),
        body: Center(
          child: Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              Text(
                "${base} : ",
                style:
                    const TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 10,
              ),
              Expanded(
                child: FutureBuilder(
                    future: getPizzasByBase(base),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        final List<Pizza> pizzas = snapshot.data!;
                        return ListView.builder(
                          itemCount: pizzas.length,
                          itemBuilder: (context, index) {
                            final Pizza pizza = pizzas[index];
                            return ListTile(
                              leading: Image.network(
                                  "$apiPizzaUrl/assets/${pizza.image}"),
                              title: Text("${pizza.name}"),
                              subtitle: Row(
                                children: [
                                  Text(
                                    "Base ${pizza.base} - ",
                                    style: const TextStyle(
                                      fontStyle: FontStyle.italic,
                                    ),
                                  ),
                                  Text("${pizza.price} €"),
                                ],
                              ),
                              trailing: IconButton(
                                icon: const Icon(Icons.add),
                                onPressed: () {
                                  Provider.of<CartManager>(context,
                                          listen: false)
                                      .addToCart(pizza);
                                },
                              ),
                              onTap: () {
                                context.go("/detail/${pizza.id}");
                              },
                            );
                          },
                        );
                      } else if (snapshot.hasError) {
                        if (kDebugMode) {
                          print(snapshot.error);
                        }
                        return const Text("Erreur");
                      } else {
                        return const CircularProgressIndicator();
                      }
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
