import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:go_router/go_router.dart';
import 'package:pizza/config.dart';
import 'package:pizza/pizza.dart';
import 'package:pizza/cart_manager.dart';
import 'package:pizza/functions_api.dart';
import 'package:provider/provider.dart';

class SearchScreen extends ChangeNotifierProvider {
  final String search;

  SearchScreen({super.key, required this.search, required super.create});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: Row(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {
                      context.go("/");
                    },
                    child: const Text(
                      "Ma Pizzeria du 54",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Consumer<CartManager>(
                      builder: (context, cart, child) {
                        return ElevatedButton(
                          child: Text(
                            "Voir mon panier (${context.watch<CartManager>().numberOfProductsChosen})",
                          ),
                          onPressed: () {
                            context.go("/cart");
                          },
                        );
                      },
                    ),
                    const SizedBox(width: 10),
                  ],
                ),
              ),
            ],
          ),
        ),
        body: Center(
          child: Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              Text(
                "Recherche \"${search}\" : ",
                style:
                    const TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
              ),
            const SizedBox(
                height: 10,
              ),
              Expanded(
                child: FutureBuilder(
                    future: getPizzasBySearch(search),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        final Pizza pizza = snapshot.data!;
                        return Column(children: [
                          Text(
                            "${pizza.name} - ${pizza.price} €",
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                            ),
                          ),
                          SizedBox(
                              width: 200.0,
                              height: 200.0,
                              child: Row(
                                children: [
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Image.network(
                                      "$apiPizzaUrl/assets/${pizza.image}"),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                ],
                              )),
                          Text(
                              "Base: ${pizza.base}\nCategory: ${pizza.category}\n"),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                              "Ingredients :\n ${pizza.ingredients?.map((ingredient) {
                            return "\n- $ingredient";
                          })}"),
                          const SizedBox(
                            height: 10,
                          ),
                          ElevatedButton(
                              onPressed: () {
                                Provider.of<CartManager>(context, listen: false)
                                    .addToCart(pizza);
                              },
                              child: const SizedBox(
                                  width: 100.0,
                                  height: 20.0,
                                  child: Row(
                                    children: [
                                      Text("Add to cart "),
                                      Icon(Icons.add),
                                    ],
                                  )))
                        ]);
                      } else if (snapshot.hasError) {
                        if (kDebugMode) {
                          print(snapshot.error);
                        }
                        return const Text("Erreur");
                      } else {
                        return const Text("Aucun résultat !");
                      }
                    }),
              ),],
          ),
        ),
      ),
    );
  }
}
