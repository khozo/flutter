import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:go_router/go_router.dart';
import 'package:pizza/cart_manager.dart';
import 'package:pizza/config.dart';
import 'package:provider/provider.dart';

class CartScreen extends ChangeNotifierProvider {
  CartScreen({super.key, required super.create});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: Row(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {
                      context.go("/");
                    },
                    child: const Text(
                      "Ma Pizzeria du 54",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Consumer<CartManager>(
                      builder: (context, cart, child) {
                        return ElevatedButton(
                          child: Text(
                            "Voir mon panier (${context.watch<CartManager>().numberOfProductsChosen})",
                          ),
                          onPressed: () {
                            context.go("/cart");
                          },
                        );
                      },
                    ),
                    const SizedBox(width: 10),
                  ],
                ),
              ),
            ],
          ),
        ),
        body: Center(
          child: Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              // Consumer<CartManager>(
              //   builder: (context, cart, child) {
              //     return Text(
              //       "Number of pizzas ${context.watch<CartManager>().numberOfProductsChosen}",
              //     );
              //   },
              // ),
              const SizedBox(
                height: 10,
              ),
              ElevatedButton(
                onPressed: () {
                  Provider.of<CartManager>(context, listen: false).removeAll();
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.red,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                ),
                child: const Text(
                  'Vider mon panier',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Expanded(
                child: Consumer<CartManager>(
                  builder: (context, cart, child) {
                    final pizzas = context.watch<CartManager>().cart.toList();
                    return ListView.builder(
                      itemCount: pizzas.length,
                      itemBuilder: (context, index) {
                        final pizza = pizzas[index];
                        return ListTile(
                          leading: Image.network(
                              "$apiPizzaUrl/assets/${pizza.image}"),
                          title: Text("${pizza.name}"),
                          trailing:
                              Text("${pizza.price?.toStringAsFixed(2)} €"),
                          onTap: () {
                            context.go("/detail/${pizza.id}");
                          },
                        );
                      },
                    );
                  },
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Consumer<CartManager>(
                builder: (context, cart, child) {
                  return Text(
                    "Prix total : ${context.watch<CartManager>().total} €",
                    style: const TextStyle(
                        fontSize: 15, fontWeight: FontWeight.bold),
                  );
                },
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
