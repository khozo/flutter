import 'package:flutter/material.dart';
import 'package:pizza/pizza.dart';

class CartManager extends ChangeNotifier {
  final List<Pizza> _cart = [];
  int _numberOfProductsChosen = 0;
  double _total = 0.0;

  List<Pizza> get cart => _cart;
  int get numberOfProductsChosen => _numberOfProductsChosen;
  double get total => _total;

  void addToCart(Pizza pizza) {
    _cart.add(pizza);
    _numberOfProductsChosen++;
    _total += pizza.price ?? 0.0;
    notifyListeners();
  }
  
  void removeAll() {
    _cart.clear();
    _numberOfProductsChosen = 0;
    _total = 0;
    notifyListeners();
  }
}