import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:pizza/config.dart';
import 'package:pizza/pizza.dart';

  Future<Pizza?> getPizza(String id) async {
    try {
      final Response response = await dioPizza.get("/items/pizzas/$id");

      return Pizza.fromJson(response.data["data"]);
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
      return null;
    }
  }

  Future<List<Pizza>?> getAllPizzas() async {
    try {
      final Response response = await dioPizza.get("/items/pizzas");

      List<dynamic> pizzaData = response.data["data"];

      List<Pizza> pizzas =
          pizzaData.map((data) => Pizza.fromJson(data)).toList();

      return pizzas;
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
      return null;
    }
  }

  Future<List<Pizza>?> getPizzasByBase(String base) async {
    try {
      final Response response = await dioPizza.get('/items/pizzas?filter={ "base": { "_eq": "$base" }}');

      List<dynamic> pizzaData = response.data["data"];

      List<Pizza> pizzas =
          pizzaData.map((data) => Pizza.fromJson(data)).toList();

      return pizzas;
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
      return null;
    }
  }

  Future<Pizza?> getPizzasBySearch(String search) async {
    try {
      final Response response = await dioPizza.get('/items/pizzas?search=$search');

      return Pizza.fromJson(response.data["data"][0]);
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
      return null;
    }
  }
