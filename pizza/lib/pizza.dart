// import 'dart:ffi';

class Pizza {
  Pizza({required this.id, this.name, this.price, this.base, this.ingredients, this.image, this.category, this.elements});

  final String id;
  String? name;
  double? price;
  String? base;
  List<dynamic>? ingredients;
  String? image;
  String? category;
  List<int>? elements;

  factory Pizza.fromJson(Map<String, dynamic> json) => Pizza(
        id: json["id"],        
        name: json["name"],
        price: json["price"],
        base: json["base"],
        ingredients: json["ingredients"],
        image: json["image"],
        category: json["category"],
        // elements: json["elements"],

  );

  @override
  String toString() {
    return 'name:$name,\n';
  }
}
