import 'package:flutter/material.dart';
import 'package:pizza/cart_manager.dart';
import 'package:pizza/my_app.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (context) => CartManager(),
      child: const MyApp(),
    ),
  );
}
