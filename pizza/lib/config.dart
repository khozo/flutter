import 'package:dio/dio.dart';
// import 'package:flutter_dotenv/flutter_dotenv.dart';

const String apiPizzaUrl = "https://pizzas.shrp.dev";

final optionsPizza = BaseOptions(baseUrl: apiPizzaUrl);
final dioPizza = Dio(optionsPizza);
