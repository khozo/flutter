import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:pizza/screens/search_screen.dart';
import 'screens/master_screen.dart';
import 'screens/detail_screen.dart';
import 'screens/cart_screen.dart';
import 'screens/base_screen.dart';

final GoRouter _router = GoRouter(
  routes: <RouteBase>[
    GoRoute(
      path: '/',
      builder: (BuildContext context, GoRouterState state) {
        return MasterScreen(create: (BuildContext context) {
          return null;
          },);
      },
    ),
    GoRoute(
      path: '/detail/:id',
      builder: (BuildContext context, GoRouterState state) {
        return DetailScreen(id: state.pathParameters['id']!, create: (BuildContext context) {
          return null;
          },
        );
      },
    ),
    GoRoute(
      path: '/list/:base',
      builder: (BuildContext context, GoRouterState state) {
        return BaseScreen(base: state.pathParameters['base']!, create: (BuildContext context) {
          return null;
          },
        );
      },
    ),
    GoRoute(
      path: '/search/:search',
      builder: (BuildContext context, GoRouterState state) {
        return SearchScreen(search: state.pathParameters['search']!, create: (BuildContext context) {
          return null;
          },
        );
      },
    ),
    GoRoute(
      path: '/cart',
      builder: (BuildContext context, GoRouterState state) {
        return CartScreen(create: (BuildContext context) {
          return null;
          },);
      },
    ),
  ],
);

class MyApp extends StatelessWidget {
  /// Constructs a [MyApp]
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerConfig: _router,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.green),
        useMaterial3: true,
      ),
    );
  }
}