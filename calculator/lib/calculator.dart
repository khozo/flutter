import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Calculator extends StatefulWidget {
  const Calculator({super.key, required this.title});

  final String title;

  @override
  State<Calculator> createState() => _CalculatorState();
}

class _CalculatorState extends State<Calculator> {
  int _counter = 0;
  int _increment = 2;
  int _numberOfClicks = 0;
  String _clickText = "";

  void _incrementCounter() {
    setState(() {
      _counter += _increment;
      _numberOfClicks++;
      _clickText = "Vous avez cliqué $_numberOfClicks fois.";
    });
  }

  void _updateIncrementValue(int incValue) {
    setState(() {
      _increment = incValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          children: [
            Text(_clickText),
            TextFormField(
              onChanged: (String value) async {
                final int incValue = int.parse(value);

                if (incValue > 0) {
                  _updateIncrementValue(incValue);
                } else {
                  await showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: const Text('Attention !'),
                        content: const Text('0 n\'est pas accepté.'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: const Text('OK'),
                          ),
                        ],
                      );
                    },
                  );
                }
              },
              keyboardType: TextInputType.number,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              decoration: const InputDecoration(
                label: Text.rich(
                  WidgetSpan(
                    child: Text(
                      'Increment',
                    ),
                  ),
                ),
                border: OutlineInputBorder(),
                hintText: 'Enter a number to increment',
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  '$_counter + $_increment = ',
                ),
                Text(
                  '${_counter + _increment}',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
              ],
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Icon(Icons.add),
            Text('$_increment'),
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
