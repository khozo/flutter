import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fruits !',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.yellow),
        useMaterial3: true,
      ),
      home: const FruitPage(title: 'Mes fruits'),
    );
  }
}

class FruitPage extends StatefulWidget {
  const FruitPage({super.key, required this.title});

  final String title;

  @override
  State<FruitPage> createState() => FruitPageState();
}

class FruitPageState extends State<FruitPage> {
  
  int _counter = 0;

  final List<String> _myFruits = [];

  void _incrementCounter() {
    setState(() {
      _counter++;
      _myFruits.add("$_counter");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(
          _type(_counter)
        ),
      ),
      body: ListView.builder(
        // the number of items in the list
        itemCount: _myFruits.length,

        // display each item of the product list
        itemBuilder: (context, index) {
          return Card(
            // In many cases, the key isn't mandatory
            key: ValueKey(_myFruits[index]),
            margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
            color: cardColor(index),
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: 
                  Row(
                    children: [
                      Text(
                        _myFruits[index],
                        style: const TextStyle(color: Colors.white),
                      ),
                      Image(
                        image: image(index+1),
                        height: 30,
                      ),
                    ],
                  )),
          );
        }
      ),
          floatingActionButton: FloatingActionButton(
            onPressed: _incrementCounter,
            tooltip: 'Increment',
            backgroundColor: buttonColor(_counter),
            child: const Icon(Icons.add),
          ),
        );
      }
    }
    
    Color cardColor(int index) {
      if(isEven(index)){
          return Colors.blue;
      }
      return Colors.yellow;
    }

    
    Color buttonColor(int index) {
      if(isEven(index)){
        return Colors.orange;
      }
      if(isOdd(index)){
          return Colors.blue;
      }
      return Colors.yellow;
    }

    AssetImage image(index){
      if(_isPrime(index)){
        return const AssetImage('images/ananas.png');
      }
      if(isEven(index)){
        return const AssetImage('images/poire.png');
      }
      return const AssetImage('images/pomme.png');
    }

    bool isEven(int val){
        return (val % 2 == 0);
    }

    bool isOdd(int val){
        return (val % 2 == 1);
    }

    bool _isPrime(int val) {
      if (val <= 1) {
        return false;
      } else {
        for (int i = 2; i < val; i++) {
          if (val % i == 0) {
            return false;
          }
        }
        return true;
      }
    }

    String _type(int val){
      if(_isPrime(val)){
        return "$val (premier)";
      }
      if(val % 2 == 0){
        return "$val (pair)";
      }
      return "$val (impair)";
    }