import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:meteo/config.dart';
import 'package:meteo/geomap.dart';
import './city.dart';
import './weather.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

String? _weatherKey = dotenv.env['WEATHER_API_KEY'];

const String apiWheatherUrl = "https://api.openweathermap.org/data/2.5/weather";

final optionsWheather = BaseOptions(baseUrl: apiWheatherUrl);
final dioWheather = Dio(optionsWheather);

class Meteo extends StatefulWidget {
  const Meteo({super.key, required this.title});

  final String title;

  @override
  State<Meteo> createState() => _MeteoState();
}

class _MeteoState extends State<Meteo> {
  City? _city;

  final _cityTfController = TextEditingController();

  @override
  void dispose() {
    _cityTfController.dispose();
    super.dispose();
  }

  Future<City?> _getCityByName(String cityName) async {
    try {
      final Response response = await dioCity.get("?name=$cityName");

      return City.fromJson(response.data[0]);
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
      return null;
    }
  }

  Future<Weather?> _getWeatherByCity(City city) async {
    try {
      final Response response = await dioWheather.get(
          "?lat=${city.latitude}&lon=${city.longitude}&units=metric&appid=$_weatherKey");

      return Weather.fromJson(response.data);
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
      return null;
    }
  }

  _getCity() async {
    City? city = await _getCityByName(_cityTfController.text);

    setState(() {
      _city = city;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Column(
            children: [
              _city?.name != null ? Text(_city!.name) : Container(),
              _city?.latitude != null
                  ? Text(_city!.latitude.toString())
                  : Container(),
            ],
          ),
        ),
        body: Center(
          child: Column(
            children: [
              Form(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TextFormField(
                      controller: _cityTfController,
                      decoration: const InputDecoration(
                        hintText: 'Enter your city',
                      ),
                      validator: (String? value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: ElevatedButton(
                        onPressed: () {
                          //_getCity();

                          setState(() {
                            _city = City(name: _cityTfController.text);
                          });
                        },
                        child: const Text('Submit'),
                      ),
                    ),
                  ],
                ),
              ),
              _city?.latitude != null
                  ? Column(
                      children: [
                        Text(_city!.name),
                        Text(_city!.latitude.toString()),
                        Text(_city!.longitude.toString()),
                      ],
                    )
                  : Container(),

              //_city?.latitude != null ? GeoMap(city: _city) : Container(),

              _city?.name == null
                  ? Container()
                  : FutureBuilder(
                      future: _getCityByName(_city!.name),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          final City city = snapshot.data!;

                          return Expanded(
                            child: Column(
                              children: [
                                Text(city.name),
                                Text(city.latitude.toString()),
                                Text(city.longitude.toString()),
                                FutureBuilder(
                                    future: _getWeatherByCity(city),
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData) {
                                        final Weather weather = snapshot.data!;
                                        return Column(
                                          children: [
                                            Text(
                                              weather.temperature.toString(),
                                            ),
                                            Text(
                                              weather.description,
                                            ),
                                          ],
                                        );
                                      } else if (snapshot.hasError) {
                                        if (kDebugMode) {
                                          print(snapshot.error);
                                        }
                                        return const Text("Erreur");
                                      } else {
                                        return const CircularProgressIndicator();
                                      }
                                    }),
                                GeoMap(city: city),
                              ],
                            ),
                          );
                        } else if (snapshot.hasError) {
                          if (kDebugMode) {
                            print(snapshot.error);
                          }
                          return const Text("Erreur");
                        } else {
                          return const CircularProgressIndicator();
                        }
                      },
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
