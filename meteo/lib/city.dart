class City {
  City({required this.name, this.longitude, this.latitude});

  final String name;
  double? longitude;
  double? latitude;

  factory City.fromJson(Map<String, dynamic> json) => City(
        name: json["name"],
        longitude: json["longitude"],
        latitude: json["latitude"],
      );

  @override
  String toString() {
    return 'name:$name,\n lon:$longitude,\n lat:$latitude';
  }
}
