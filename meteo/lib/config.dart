import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';


String? _cityKey = dotenv.env['CITY_API_KEY'];

const String apiCityUrl = "https://api.api-ninjas.com/v1/city";

final optionsCity = BaseOptions(baseUrl: apiCityUrl, headers: {"X-Api-Key": "$_cityKey"});
final dioCity = Dio(optionsCity);



