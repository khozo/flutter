import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:flutter/material.dart';
import 'package:meteo/city.dart';
import 'package:url_launcher/url_launcher.dart';

class GeoMap extends StatelessWidget {
  const GeoMap({
    super.key,
    required this.city,
  });

  final City? city;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: FlutterMap(
        key: UniqueKey(),
        mapController: MapController(),
        options: MapOptions(
          initialCenter: LatLng(
            city!.latitude!,
            city!.longitude!,
          ),
          initialZoom: 9.5,
        ),
        children: [
          TileLayer(
            urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
            userAgentPackageName: 'com.example.app',
          ),
          RichAttributionWidget(
            attributions: [
              TextSourceAttribution(
                'OpenStreetMap contributors',
                onTap: () =>
                    launchUrl(Uri.parse('https://openstreetmap.org/copyright')),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
