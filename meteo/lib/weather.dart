class Weather {
  Weather({required this.temperature, required this.description});

  final double temperature;
  final String description;

  factory Weather.fromJson(Map<String, dynamic> json) => Weather(
        temperature: json['main']['temp'],
        description: json['weather'][0]["description"],
    );

    @override
    String toString() {
      return 'temperature:$temperature,\n description:"$description"';
    }
}
