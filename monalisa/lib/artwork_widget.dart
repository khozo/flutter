import 'package:flutter/material.dart';

class ArtworkWidget extends StatefulWidget {
  const ArtworkWidget({super.key, required this.title});

  final String title;

  @override
  State<ArtworkWidget> createState() => _ArtworkWidgetState();
}

class _ArtworkWidgetState extends State<ArtworkWidget>{

  bool _isFavorite = false;
  Color _color = Colors.white.withOpacity(0.75);
  Color _favoriteButtonColor = Colors.brown; 
  String _snackBar = "Retiré au favori";
  double _opacity = 0.0;

  void _favorite() {
    setState(() {
                  
      if(!_isFavorite){
        _color = Colors.red.withOpacity(1.0);
        _favoriteButtonColor = Colors.red; 
        _snackBar = "Ajouté des favoris";
      }else{
        _color = Colors.white.withOpacity(0.75);
        _favoriteButtonColor = Colors.brown; 
        _snackBar = "Retiré aux favoris";
      }
      
      var snackBar = SnackBar(
          content: Text(_snackBar),
          backgroundColor: (Colors.brown),
          action: SnackBarAction(
            label: 'Compris',
            onPressed: () {
            },
          ),
        );
      _isFavorite = !_isFavorite;

      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      
    });
  }

  void _article() {
    setState(() {
      if(_opacity == 0.0){
        _opacity = 1.0;
      }else{
        _opacity = 0.0;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text("Mona Lisa"),
      ),
      body: Center(
        child: Column(
          children: [ 
            Center(
              child: Stack(
                children: [
                  const Center(
                    child: Image(
                      image: AssetImage('images/Mona_Lisa.jpg')
                    ),
                  ),
                  Positioned.fill(
                    child: Align(
                      alignment: Alignment.center,
                      child:  Icon(
                        Icons.favorite,
                        color: _color,
                        size: 100,
                      )
                    )
                  ),
                  Positioned.fill(
                    child: Align(
                      alignment: Alignment.center,
                      child:
                       Text(
                        "La Joconde, ou Portrait de Mona Lisa, est un tableau de l'artiste Léonard de Vinci, réalisé entre 1503 et 1506 ou entre 1513 et 15161,2, et peut-être jusqu'à 1517 (l'artiste étant mort le 2 mai 1519), qui représente un portrait mi-corps, probablement celui de la Florentine Lisa Gherardini, épouse de Francesco del Giocondo. Acquise par François Ier, cette peinture à l'huile sur panneau de bois de peuplier de 77 × 53 cm est exposée au musée du Louvre à Paris. La Joconde est l'un des rares tableaux attribués de façon certaine à Léonard de Vinci. La Joconde est devenue un tableau éminemment célèbre car, depuis sa réalisation, nombre d'artistes l'ont pris comme référence. À l'époque romantique, les artistes ont été fascinés par ce tableau et ont contribué à développer le mythe qui l'entoure, en faisant de ce tableau l’une des œuvres d'art les plus célèbres du monde, si ce n'est la plus célèbre : elle est en tout cas considérée comme l'une des représentations d'un visage féminin les plus célèbres au monde. Au xxie siècle, elle est devenue l'objet d'art le plus visité au monde, devant le diamant Hope, avec 20 000 visiteurs qui viennent l'admirer et la photographier quotidiennement",
                        overflow: TextOverflow.clip,
                        style: TextStyle(
                          // backgroundColor: Colors.grey,
                          color: Colors.white.withOpacity(_opacity),
                          ),
                        ),
                    ),
                  ),
                ]
              ),
            ),
            const Text(
              "Mona Lisa",
              style: TextStyle(
                fontFamily: 'Merriweather',
                fontSize: 30,
                color: Colors.brown,
                ),
              ),
            const Text(
              "Léonard De Vinci",
              style: TextStyle(
                fontFamily: 'Merriweather',
                fontSize: 15,
                fontWeight: FontWeight.bold,
                color: Colors.brown,
              )
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(
                  icon: const Icon(
                    Icons.article, 
                  ),
                  onPressed: _article 
                ),
                IconButton(
                  icon: const Icon(
                    Icons.favorite,
                  ),
                  color: _favoriteButtonColor,
                  onPressed: _favorite
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
